import React, { useContext, useRef, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { UserContext } from "../App";
import { MDBBtn, MDBIcon } from "mdbreact";
import M from "materialize-css";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

const NavBar = () => {
  const searchModal = useRef(null);
  const [search, setSearch] = useState("");
  const [userDetails, setUserDetails] = useState([]);
  const { state, dispatch } = useContext(UserContext);
  const history = useHistory();
  useEffect(() => {
    M.Modal.init(searchModal.current);
  }, []);

  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  const renderList = () => {
    if (state) {
      return [
        <li key="1" className="custom_li_nav">
          <li>
            {windowDimensions.width < 541 ? (
              <Link className="like_text_nav" to="/profile">
                <MDBIcon
                  style={{ current: "pointer" }}
                  far
                  icon="user-circle"
                />
              </Link>
            ) : (
              <Link className="like_text_nav" to="/profile">
                Profile
              </Link>
            )}
          </li>
          <li>
            {windowDimensions.width < 541 ? (
              <Link className="like_text_nav" to="/create">
                <MDBIcon icon="plus-circle" />
              </Link>
            ) : (
              <Link className="like_text_nav" to="/create">
                Create Post
              </Link>
            )}
          </li>
          <li>
            {windowDimensions.width < 541 ? (
              <Link className="like_text_nav" to="/myfollowingpost">
                <MDBIcon far icon="check-circle" />
              </Link>
            ) : (
              <Link className="like_text_nav" to="/myfollowingpost">
                My following Posts
              </Link>
            )}
          </li>
        </li>,
        <li key="2" className="custom_li_logout_search">
          <li>
            {windowDimensions.width < 541 ? (
              <MDBIcon
                data-target="modal1"
                className="custom_icon_size mr-auto modal-trigger"
                style={{ color: "black" }}
                icon="search"
              />
            ) : (
              <MDBBtn
                data-target="modal1"
                className="mr-auto modal-trigger"
                outline
                color="warning"
                rounded
                size="sm"
                type="submit"
              >
                Search
              </MDBBtn>
            )}
          </li>
          ,
          <li className="custom_logout_user">
            {windowDimensions.width < 541 ? (
              <MDBIcon
                className="custom_icon_size"
                onClick={() => {
                  localStorage.clear();
                  dispatch({ type: "CLEAR" });
                  history.push("/signin");
                }}
                style={{ color: "black" }}
                icon="sign-out-alt"
              />
            ) : (
              <MDBBtn
                color="danger"
                rounded
                size="sm"
                type="submit"
                onClick={() => {
                  localStorage.clear();
                  dispatch({ type: "CLEAR" });
                  history.push("/signin");
                }}
              >
                Logout
              </MDBBtn>
            )}
          </li>
          ,
        </li>,
      ];
    } else {
      return [
        <li key="3">
          <Link to="/signin">
            <MDBBtn className="custom_btn_login" gradient="blue">
              Signin
            </MDBBtn>
          </Link>
        </li>,
        <li key="4">
          <Link to="/signup">
            <MDBBtn className="custom_btn_login" gradient="aqua">
              Signup
            </MDBBtn>
          </Link>
        </li>,
      ];
    }
  };

  const fetchUsers = (query) => {
    setSearch(query);
    fetch("/search-users", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query,
      }),
    })
      .then((res) => res.json())
      .then((results) => {
        setUserDetails(results.user);
      });
  };
  return (
    <div className="">
      <nav className="white">
        <div className="nav-wrapper container">
          <Link to={state ? "/" : "/signin"} className="brand-logo left">
            Twitter
          </Link>
          <ul id="nav-mobile" className="right custom_right">
            {renderList()}
          </ul>
        </div>
      </nav>
      <div
        id="modal1"
        className="modal"
        ref={searchModal}
        style={{ color: "black" }}
      >
        <div className="modal-content">
          <input
            type="text"
            placeholder="search users"
            value={search}
            onChange={(e) => fetchUsers(e.target.value)}
          />
          <ul className="collection">
            {userDetails.map((item) => {
              return (
                <Link
                  key={item._id}
                  to={
                    item._id !== state._id ? "/profile/" + item._id : "/profile"
                  }
                  onClick={() => {
                    M.Modal.getInstance(searchModal.current).close();
                    setSearch("");
                  }}
                >
                  <li className="collection-item">{item.name}</li>
                </Link>
              );
            })}
          </ul>
        </div>
        <div className="modal-footer">
          <button
            className="modal-close waves-effect waves-green btn-flat"
            onClick={() => setSearch("")}
          >
            close
          </button>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
