import React, { useState, useEffect } from "react";
import M from "materialize-css";
import { useHistory } from "react-router-dom";
import { MDBBtn, MDBIcon, MDBInput } from "mdbreact";
const CretePost = () => {
  const history = useHistory();
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");

  const updatePhoto = (file) => {
    setImage(file);
  };

  useEffect(() => {
    if (image) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "twitter");
      data.append("cloud_name", "dqsjs4uyz");
      fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setUrl(data.url);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [image]);

  const postDetails = () => {
    fetch("/createpost", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        title,
        body,
        pic: url,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          M.toast({ html: data.error, classes: "#c62828 red darken-3" });
        } else {
          M.toast({
            html: "Created post Successfully",
            classes: "#43a047 green darken-1",
          });
          history.push("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div
      className="card input-filed"
      style={{
        margin: "30px auto",
        maxWidth: "80%",
        padding: "20px",
        textAlign: "center",
      }}
    >
      <h4>Create post</h4>
      <input
        type="text"
        placeholder="title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <input
        type="text"
        placeholder="body"
        value={body}
        onChange={(e) => setBody(e.target.value)}
      />
      <div className="">
        <label className="custom_update_image_avatar" htmlFor="upload">
          <MDBIcon style={{ fontSize: "1.5rem" }} far icon="images" />
        </label>

        {!url ? (
          <div className="d-flex">
            <span className="d-flex">Đăng ảnh</span>
          </div>
        ) : (
          <div className="d-flex justify-content-center">
            <img
              width="50%"
              height="50%"
              className="image_custom_create"
              src={url}
              alt="image_edit"
            />
          </div>
        )}
        <MDBInput
          type="file"
          hidden
          id="upload"
          onChange={(e) => updatePhoto(e.target.files[0])}
        />
      </div>
      <MDBBtn
        style={{ lineHeight: 0 }}
        onClick={() => postDetails()}
        outline
        color="success"
      >
        submit post
      </MDBBtn>
    </div>
  );
};

export default CretePost;
