import React, { useEffect, useState, useContext, useCallback } from "react";
import { UserContext } from "../../App";
import { Link, useParams } from "react-router-dom";
import { MDBBtn, MDBIcon, MDBInput } from "mdbreact";
import moment from "moment";
const Profile = () => {
  const [userProfile, setuserProfile] = useState([]);
  const [postProfile, setpostProfile] = useState([]);
  const { state, dispatch } = useContext(UserContext);
  const { userid } = useParams();

  const getUser = useCallback(async () => {
    await fetch(`/user/${userid}`, {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setuserProfile(result.user);
        setpostProfile(result.posts);
      });
  }, [userid]);

  useEffect(() => {
    getUser();
  }, [getUser]);
  const people = state ? state._id : "";
  const followers = userProfile.followers;
  const checkfollowers = followers ? followers.includes(people) : false;
  const followUser = () => {
    fetch("/follow", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        followId: userid,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        dispatch({
          type: "UPDATE",
          payload: { following: data.following, followers: data.followers },
        });
        localStorage.setItem("user", JSON.stringify(data));
        setuserProfile((prevState) => {
          return {
            ...prevState,
            followers: [...prevState.followers, data._id],
          };
        });
      });
  };
  const unfollowUser = () => {
    fetch("/unfollow", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        unfollowId: userid,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        dispatch({
          type: "UPDATE",
          payload: { following: data.following, followers: data.followers },
        });
        localStorage.setItem("user", JSON.stringify(data));

        setuserProfile((prevState) => {
          const newFollower = prevState.followers.filter(
            (item) => item !== data._id
          );
          return {
            ...prevState,
            followers: newFollower,
          };
        });
      });
  };

  const likePost = (id, index) => {
    fetch("/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = postProfile.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setpostProfile(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const unlikePost = (id, index) => {
    fetch("/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = postProfile.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setpostProfile(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };

  const [pustComment, setpustComment] = useState({ text: "" });
  const onChangecmt = (e) => {
    setpustComment(e.target.value);
  };
  const makeComment = (e, postId) => {
    if (e.key === "Enter") {
      fetch("/comment", {
        method: "put",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("jwt"),
        },
        body: JSON.stringify({
          postId,
          text: pustComment,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          console.log(result);
          const newData = postProfile.map((item) => {
            if (item._id === result._id) {
              return result;
            } else {
              return item;
            }
          });
          setpostProfile(newData);
          setpustComment({ text: "" });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <div>
      {userProfile ? (
        <div className="custom_list_main_profile">
          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              margin: "18px 0px",
              borderBottom: "1px solid grey",
            }}
          >
            <div className="custom_view_image">
              <img
                className="profile_image_custom"
                src={userProfile.pic}
                alt="photohihi"
              />
            </div>
            <div>
              <h4
                className="custom_name_profile"
                style={{
                  fontWeight: 600,
                  textTransform: "uppercase",
                }}
              >
                {userProfile.name}
              </h4>
              <h5 className="custom_email_profile">{userProfile.email}</h5>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "108%",
                }}
              >
                <h6 className="custom_length_profile">
                  {postProfile ? postProfile.length : ""} posts
                </h6>
                <h6 className="custom_length_profile">
                  {userProfile.followers ? userProfile.followers.length : ""}{" "}
                  followers
                </h6>
                <h6 className="custom_length_profile">
                  {userProfile.following ? userProfile.following.length : ""}{" "}
                  following
                </h6>
              </div>
              {!checkfollowers ? (
                <MDBBtn
                  className="d-flex align-items-center"
                  outline
                  color="success"
                  size="sm"
                  onClick={() => followUser()}
                >
                  Follow{" "}
                  <MDBIcon style={{ fontSize: "1rem" }} icon="user-check" />
                </MDBBtn>
              ) : (
                <MDBBtn
                  className="d-flex align-items-center"
                  outline
                  color="danger"
                  size="sm"
                  onClick={() => unfollowUser()}
                >
                  UnFollow{" "}
                  <MDBIcon style={{ fontSize: "1rem" }} icon="user-times" />
                </MDBBtn>
              )}
            </div>
          </div>

          <div className="custom_profile_list">
            {postProfile.map((item, index) => {
              return (
                <div key={item._id} className="render_list_card_profile">
                  <div className="card home-card">
                    <div
                      className="d-flex justify-content-lg-between align-items-center"
                      style={{ padding: "5px" }}
                    >
                      <div className="d-flex justify-content-lg-between align-items-center">
                        <span className="custom_name_postedPy">
                          {item.postedBy.name}
                        </span>
                        <span className="custom_time_format">
                          {formatDate(item.createdAt)}
                        </span>
                      </div>
                    </div>
                    <div className="card-image d-flex justify-content-center">
                      <img
                        style={{ width: "65%", height: "50%" }}
                        src={item.photo}
                        alt="photoblabla"
                      />
                    </div>
                    <div className="card-content">
                      <div
                        className="d-flex justify-content-between"
                        style={{ height: 25 }}
                      >
                        <div className="d-flex align-items-center">
                          <i
                            className={
                              item.likes.includes(state._id)
                                ? "material-icons text-danger"
                                : "material-icons text-dark"
                            }
                          >
                            favorite
                          </i>
                          {item.likes.includes(state._id) ? (
                            <MDBIcon
                              style={{ fontSize: "1.1rem", cursor: "pointer" }}
                              onClick={() => {
                                unlikePost(item._id, index);
                              }}
                              far
                              icon="thumbs-down"
                            />
                          ) : (
                            <MDBIcon
                              style={{ fontSize: "1.1rem", cursor: "pointer" }}
                              onClick={() => {
                                likePost(item._id, index);
                              }}
                              far
                              icon="thumbs-up"
                            />
                          )}
                        </div>
                        <label htmlFor={item._id}>
                          <div
                            className="d-flex align-items-center"
                            style={{
                              position: "relative",
                              right: 30,
                              cursor: "pointer",
                              fontSize: "1rem",
                              color: "black",
                              top: 6,
                            }}
                          >
                            <MDBIcon far icon="comment-alt" />
                          </div>
                        </label>
                      </div>
                      <div className="d-flex justify-content-between">
                        <h6
                          className="custom_like_and_cmt"
                          style={{ fontSize: "0.8rem", fontWeight: 600 }}
                        >
                          {item.likes.length} likes
                        </h6>
                        <h6 style={{ fontSize: "0.8rem", fontWeight: 600 }}>
                          {item.comments.length} comment
                        </h6>
                      </div>
                      <h6 className="custom_body_text">{item.title}</h6>
                      <p className="custom_title_text_body">{item.body}</p>
                      <div style={{ top: "10px", position: "relative" }}>
                        {item.comments.map((record) => {
                          return (
                            <h6 key={record._id}>
                              <Link
                                className="custom_text_cmt"
                                to={
                                  record.postedBy._id !== state._id
                                    ? "/profile/" + record.postedBy._id
                                    : "/profile"
                                }
                              >
                                {record.postedBy.name}
                              </Link>
                              <span className="custom_cmt_text_user">
                                {record.text}
                              </span>
                            </h6>
                          );
                        })}
                      </div>
                      <MDBInput
                        style={{ height: "2.3rem", fontSize: "0.8rem" }}
                        className="custom_input_comment_send"
                        label="add a comment"
                        id={item._id}
                        onKeyDown={(e) => makeComment(e, item._id)}
                        name={item._id}
                        value={pustComment.text}
                        onChange={onChangecmt}
                        type="text"
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      ) : (
        <h2>loading...!</h2>
      )}
    </div>
  );
};

export default Profile;
