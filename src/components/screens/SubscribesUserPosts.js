import React, { useState, useEffect, useContext } from "react";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import { MDBIcon, MDBInput } from "mdbreact";
import "react-responsive-modal/styles.css";
import moment from "moment";
export default function Home() {
  const [data, setData] = useState([]);
  const { state } = useContext(UserContext);
  useEffect(() => {
    fetch("/getsubpost", {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setData(result.posts);
      });
  }, []);

  const likePost = (id) => {
    fetch("/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const unlikePost = (id) => {
    fetch("/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const [pustComment, setpustComment] = useState({ text: "" });
  const onChangecmt = (e) => {
    setpustComment(e.target.value);
  };
  const makeComment = (e, postId) => {
    if (e.key === "Enter") {
      fetch("/comment", {
        method: "put",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("jwt"),
        },
        body: JSON.stringify({
          postId,
          text: pustComment,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          console.log(result);
          const newData = data.map((item) => {
            if (item._id === result._id) {
              return result;
            } else {
              return item;
            }
          });
          setData(newData);
          setpustComment({ text: "" });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };
  return (
    <div>
      <div className="home">
        {data.map((item) => {
          return (
            <div className="card home-card" key={item._id}>
              <div
                className="d-flex justify-content-lg-between align-items-center"
                style={{ padding: "5px" }}
              >
                <div className="d-flex justify-content-lg-between align-items-center">
                  <Link
                    className="custom_name_postedPy"
                    to={
                      item.postedBy._id !== state._id
                        ? "/profile/" + item.postedBy._id
                        : "/profile"
                    }
                  >
                    {item.postedBy.name}
                  </Link>
                  <span className="custom_time_format">
                    {formatDate(item.createdAt)}
                  </span>
                </div>
              </div>
              <div className="card-image d-flex justify-content-center">
                <img
                  style={{ width: "65%", height: "50%" }}
                  src={item.photo}
                  alt="photoblabla"
                />
              </div>
              <div className="card-content">
                <div
                  className="d-flex justify-content-between"
                  style={{ height: 25 }}
                >
                  <div className="d-flex align-items-center">
                    <i
                      className={
                        item.likes.includes(state._id)
                          ? "material-icons text-danger"
                          : "material-icons text-dark"
                      }
                    >
                      favorite
                    </i>
                    {item.likes.includes(state._id) ? (
                      <MDBIcon
                        style={{ fontSize: "1.1rem", cursor: "pointer" }}
                        onClick={() => {
                          unlikePost(item._id);
                        }}
                        far
                        icon="thumbs-down"
                      />
                    ) : (
                      <MDBIcon
                        style={{ fontSize: "1.1rem", cursor: "pointer" }}
                        onClick={() => {
                          likePost(item._id);
                        }}
                        far
                        icon="thumbs-up"
                      />
                    )}
                  </div>
                  <label htmlFor={item._id}>
                    <div
                      className="d-flex align-items-center"
                      style={{
                        position: "relative",
                        right: 30,
                        cursor: "pointer",
                        fontSize: "1rem",
                        color: "black",
                        top: 6,
                      }}
                    >
                      <MDBIcon far icon="comment-alt" />
                    </div>
                  </label>
                </div>
                <div className="d-flex justify-content-between">
                  <h6 style={{ fontSize: "0.8rem", fontWeight: 600 }}>
                    {item.likes.length} likes
                  </h6>
                  <h6 style={{ fontSize: "0.8rem", fontWeight: 600 }}>
                    {item.comments.length} comment
                  </h6>
                </div>
                <h6 className="custom_body_text">{item.title}</h6>
                <p className="custom_title_text_body">{item.body}</p>
                <div style={{ top: "10px", position: "relative" }}>
                  {item.comments.map((record) => {
                    return (
                      <h6 key={record._id}>
                        <Link
                          className="custom_text_cmt"
                          to={
                            record.postedBy._id !== state._id
                              ? "/profile/" + record.postedBy._id
                              : "/profile"
                          }
                        >
                          {record.postedBy.name}
                        </Link>{" "}
                        <span className="custom_cmt_text_user">
                          {record.text}
                        </span>
                      </h6>
                    );
                  })}
                </div>
                <MDBInput
                  style={{ height: "2.3rem", fontSize: "0.8rem" }}
                  className="custom_input_comment_send"
                  label="add a comment"
                  id={item._id}
                  onKeyDown={(e) => makeComment(e, item._id)}
                  name={item._id}
                  value={pustComment.text}
                  onChange={onChangecmt}
                  type="text"
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
