import React, { useState, useEffect, useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import M from "materialize-css";
import {
  MDBBtn,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBRow,
} from "mdbreact";
const SignIn = () => {
  const history = useHistory();
  const [name, setName] = useState("");
  const [password, setPasword] = useState("");
  const [email, setEmail] = useState("");
  const [image, setImage] = useState("");
  const [url, setUrl] = useState(undefined);

  const uploadPic = useCallback(() => {
    const data = new FormData();
    data.append("file", image);
    data.append("upload_preset", "twitter");
    data.append("cloud_name", "dqsjs4uyz");
    fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
      method: "post",
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        setUrl(data.url);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [image]);
  const uploadFields = useCallback(() => {
    if (
      !/^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      M.toast({ html: "invalid email", classes: "#c62828 red darken-3" });
      return;
    }
    fetch("/signup", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        password,
        email,
        pic: url,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          M.toast({ html: data.error, classes: "#c62828 red darken-3" });
        } else {
          M.toast({ html: data.message, classes: "#43a047 green darken-1" });
          history.push("/signin");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [email, history, name, password, url]);
  useEffect(() => {
    if (image) uploadPic();
  }, [image, uploadPic]);
  const PostData = () => {
    uploadFields();
  };

  return (
    <MDBContainer style={{ position: "relative", top: 50 }}>
      <MDBRow className="justify-content-md-center">
        <MDBCol md="6">
          <form>
            <p className="h5 text-center mb-4">Sign up</p>
            <div className="grey-text">
              <MDBInput
                label="Your name"
                icon="user"
                group
                type="text"
                validate
                error="wrong"
                success="right"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <MDBInput
                label="Your email"
                icon="envelope"
                group
                type="email"
                validate
                error="wrong"
                success="right"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <MDBInput
                label="Your password"
                icon="lock"
                group
                type="password"
                validate
                value={password}
                onChange={(e) => setPasword(e.target.value)}
              />
              <div className="">
                <label className="custom_update_image_avatar" htmlFor="upload">
                  <MDBIcon style={{ fontSize: "1.5rem" }} far icon="images" />
                </label>

                {!url ? (
                  <div className="d-flex">
                    <span className="d-flex">avatar</span>
                  </div>
                ) : (
                  <div className="d-flex justify-content-center">
                    <img width="20%" src={url} alt="image_edit" />
                  </div>
                )}
                <MDBInput
                  type="file"
                  hidden
                  id="upload"
                  onChange={(e) => setImage(e.target.files[0])}
                />
              </div>
            </div>
            <div className="text-center">
              <MDBBtn
                onClick={() => PostData()}
                className="custom_btn_login custom_btn_logintrue"
                gradient="aqua"
              >
                <span className="custom_text_login ">SignUP</span>
              </MDBBtn>
              <h5>
                <Link className="dont_have_account" to="/signin">
                  Already have an account ?
                </Link>
              </h5>
            </div>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default SignIn;
