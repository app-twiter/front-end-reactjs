import React, { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import M from "materialize-css";
import { MDBBtn, MDBCol, MDBContainer, MDBInput, MDBRow } from "mdbreact";
const SignIn = () => {
  const history = useHistory();
  const [password, setPasword] = useState("");
  const { token } = useParams();
  console.log(token);
  const PostData = () => {
    fetch("/new-password", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        password,
        token,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.error) {
          M.toast({ html: data.error, classes: "#c62828 red darken-3" });
        } else {
          M.toast({ html: data.message, classes: "#43a047 green darken-1" });
          history.push("/signin");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <MDBContainer style={{ position: "relative", top: 50 }}>
      <MDBRow className="justify-content-md-center">
        <MDBCol md="6">
          <form>
            <p className="h5 text-center mb-4">Update new password</p>
            <div className="grey-text">
              <MDBInput
                label="Enter new password"
                icon="lock"
                group
                type="password"
                value={password}
                onChange={(e) => setPasword(e.target.value)}
              />
            </div>
            <div className="text-center">
              <MDBBtn
                onClick={() => PostData()}
                className="custom_btn_login custom_btn_logintrue"
                gradient="aqua"
              >
                <span className="custom_text_login"> Update password</span>
              </MDBBtn>
            </div>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default SignIn;
