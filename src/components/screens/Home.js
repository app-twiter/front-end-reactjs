import React, { useState, useEffect, useContext, useCallback } from "react";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import { MDBBtn, MDBIcon, MDBInput } from "mdbreact";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import moment from "moment";

export default function Home() {
  const [data, setData] = useState([]);
  const { state } = useContext(UserContext);
  const getAllPost = useCallback(async () => {
    await fetch("/allpost", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        setData(result.posts);
      });
  }, []);
  useEffect(() => {
    getAllPost();
  }, [getAllPost]);

  const likePost = (id) => {
    fetch("/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const unlikePost = (id) => {
    fetch("/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const [pustComment, setpustComment] = useState({ text: "" });
  const onChangecmt = (e) => {
    setpustComment(e.target.value);
  };
  const makeComment = (e, postId) => {
    if (e.key === "Enter") {
      fetch("/comment", {
        method: "put",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("jwt"),
        },
        body: JSON.stringify({
          postId,
          text: pustComment,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          console.log(result);
          const newData = data.map((item) => {
            if (item._id === result._id) {
              return result;
            } else {
              return item;
            }
          });
          setData(newData);
          setpustComment({ text: "" });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const deletePost = (postid) => {
    fetch(`/deletepost/${postid}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.filter((item) => {
          return item._id !== result._id;
        });
        setData(newData);
      });
  };

  const [open, setOpen] = useState(false);
  const [postdeleteId, setpostdeleteId] = useState();
  const onOpenModalDelete = (postId) => {
    setpostdeleteId(postId);
    setOpen(true);
  };
  const deletePostOK = () => {
    try {
      deletePost(postdeleteId);
      setOpen(false);
    } catch (error) {
      console.log(error);
    }
  };
  const cancelDelete = () => {
    setOpen(false);
  };
  const onCloseModal = () => setOpen(false);

  const [open1, setOpen1] = useState(false);
  const [postEditId, setpostEditId] = useState();
  const [body, setbody] = useState();
  const [photo, setphoto] = useState();
  const [title, settitle] = useState();
  const [url, setUrl] = useState();
  const onOpenModalEdit = (postId, body, photo, title) => {
    setbody(body);
    setphoto(photo);
    settitle(title);
    setpostEditId(postId);
    setOpen1(true);
  };
  const cancelEdit = () => {
    setOpen1(false);
  };

  const editPost = (postid) => {
    fetch(`/updatepost/${postid}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postid,
        body: body,
        photo: url,
        title: title,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
        setpustComment("");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onCloseModalEdit = () => setOpen1(false);
  const editPostOK = () => {
    try {
      editPost(postEditId);
      setOpen1(false);
      setUrl("");
    } catch (error) {
      console.log(error);
    }
  };

  const [image, setImage] = useState();
  const updatePhoto = (file) => {
    setImage(file);
  };

  useEffect(() => {
    const data = new FormData();
    data.append("file", image);
    data.append("upload_preset", "twitter");
    data.append("cloud_name", "dqsjs4uyz");
    fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
      method: "post",
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        setUrl(data.url);
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {};
  }, [image]);
  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };
  return (
    <div>
      <Modal open={open1} onClose={onCloseModalEdit} top>
        <div className="custom_show_edit_model">
          <h4 style={{ textAlign: "center", fontWeight: 600 }}>Edit post?</h4>
          <MDBInput
            onChange={(e) => settitle(e.target.value)}
            label="Edit title"
            group
            type="text"
            value={title}
          />
          <MDBInput
            onChange={(e) => setbody(e.target.value)}
            label="Edit body"
            group
            type="text"
            value={body}
          />
          <span style={{ fontSize: "0.85rem", color: "#757575" }}>
            Edit ảnh
          </span>
          <label className="custom_update_image_avatar" htmlFor="upload">
            <MDBIcon far icon="edit" />
          </label>
          {url ? (
            <div className="d-flex justify-content-center">
              <img width="50%" height="50%" src={url} alt="image_edit" />
            </div>
          ) : (
            <div className="d-flex justify-content-center">
              <img width="50%" height="50%" src={photo} alt="image_edit" />
            </div>
          )}

          <MDBInput
            type="file"
            hidden
            id="upload"
            onChange={(e) => updatePhoto(e.target.files[0])}
          />
          <div className="d-flex justify-content-center">
            <MDBBtn
              onClick={editPostOK}
              className="custom_btn_show_modal"
              outline
              color="success"
            >
              OK
            </MDBBtn>
            <MDBBtn
              onClick={cancelEdit}
              className="custom_btn_show_modal"
              outline
              color="danger"
            >
              Cancel
            </MDBBtn>
          </div>
        </div>
      </Modal>

      <Modal open={open} onClose={onCloseModal} center>
        <h2>Are your sure?</h2>
        <MDBBtn
          onClick={deletePostOK}
          className="custom_btn_show_modal"
          outline
          color="success"
        >
          OK
        </MDBBtn>
        <MDBBtn
          onClick={cancelDelete}
          className="custom_btn_show_modal"
          outline
          color="danger"
        >
          Cancel
        </MDBBtn>
      </Modal>
      <div className="home">
        {data.map((item) => {
          return (
            <div className="card home-card" key={item._id}>
              <div
                className="d-flex justify-content-between align-items-center"
                style={{ padding: "5px" }}
              >
                <div className="d-flex justify-content-between align-items-center">
                  <Link
                    className="custom_name_postedPy"
                    to={
                      item.postedBy._id !== state._id
                        ? "/profile/" + item.postedBy._id
                        : "/profile"
                    }
                  >
                    {item.postedBy.name}
                  </Link>
                  <span className="custom_time_format">
                    {formatDate(item.createdAt)}
                  </span>
                </div>
                {item.postedBy._id === state._id && (
                  <div className="custom_option_edit_delete">
                    <MDBIcon
                      style={{ cursor: "pointer" }}
                      far
                      icon="edit"
                      onClick={() =>
                        onOpenModalEdit(
                          item._id,
                          item.body,
                          item.photo,
                          item.title
                        )
                      }
                    />
                    <MDBIcon
                      style={{ cursor: "pointer" }}
                      far
                      icon="trash-alt"
                      onClick={() => onOpenModalDelete(item._id)}
                    />
                  </div>
                )}
              </div>
              <div className="card-image d-flex justify-content-center">
                <img
                  style={{ width: "65%", height: "50%" }}
                  src={item.photo}
                  alt="photoblabla"
                />
              </div>
              <div className="card-content">
                <div
                  className="d-flex justify-content-between"
                  style={{ height: 25 }}
                >
                  <div className="d-flex align-items-center">
                    <i
                      className={
                        item.likes.includes(state._id)
                          ? "material-icons text-danger"
                          : "material-icons text-dark"
                      }
                    >
                      favorite
                    </i>
                    {item.likes.includes(state._id) ? (
                      <MDBIcon
                        style={{ fontSize: "1.1rem", cursor: "pointer" }}
                        onClick={() => {
                          unlikePost(item._id);
                        }}
                        far
                        icon="thumbs-down"
                      />
                    ) : (
                      <MDBIcon
                        style={{ fontSize: "1.1rem", cursor: "pointer" }}
                        onClick={() => {
                          likePost(item._id);
                        }}
                        far
                        icon="thumbs-up"
                      />
                    )}
                  </div>
                  <label htmlFor={item._id}>
                    <div
                      className="d-flex align-items-center"
                      style={{
                        position: "relative",
                        right: 30,
                        cursor: "pointer",
                        fontSize: "1rem",
                        color: "black",
                        top: 6,
                      }}
                    >
                      <MDBIcon far icon="comment-alt" />
                    </div>
                  </label>
                </div>
                <div className="d-flex justify-content-between">
                  <h6 style={{ fontSize: "0.8rem", fontWeight: 600 }}>
                    {item.likes.length} likes
                  </h6>
                  <h6 style={{ fontSize: "0.8rem", fontWeight: 600 }}>
                    {item.comments.length} comment
                  </h6>
                </div>
                <h6 className="custom_body_text">{item.title}</h6>
                <p className="custom_title_text_body">{item.body}</p>
                <div style={{ top: "10px", position: "relative" }}>
                  {item.comments.map((record) => {
                    return (
                      <h6 key={record._id}>
                        <Link
                          className="custom_text_cmt"
                          to={
                            record.postedBy._id !== state._id
                              ? "/profile/" + record.postedBy._id
                              : "/profile"
                          }
                        >
                          {record.postedBy.name}
                        </Link>{" "}
                        <span className="custom_cmt_text_user">
                          {record.text}
                        </span>
                      </h6>
                    );
                  })}
                </div>
                <MDBInput
                  style={{ height: "2.3rem", fontSize: "0.8rem" }}
                  className="custom_input_comment_send"
                  label="add a comment"
                  id={item._id}
                  onKeyDown={(e) => makeComment(e, item._id)}
                  name={item._id}
                  value={pustComment.text}
                  onChange={onChangecmt}
                  type="text"
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
